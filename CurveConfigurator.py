import tkinter as tk

import numpy as np
from CurvePlot import CurvePlotFrame
from EnvelopePlot import EnvelopePlotFrame, Point
from Operator import FMOperator
from NumericEntry import NumericEntry
from Envelope import Envelope

default_frequency = 440
default_amplitude = 1
default_phase = 0


class CurveConfigurator(tk.Frame):
    def __init__(self, master, operatorType, duration, samplerate, command=lambda curve: None, color=None, **kwargs):
        super().__init__(master, **kwargs)

        self.envelope = Envelope()
        self.operator: FMOperator = operatorType(
            default_frequency,
            default_amplitude,
            default_phase,
            self.envelope
        )
        self.onChange = command
        self.frequencyVariable = tk.DoubleVar(value=self.operator.frequency)
        sliderFreq = self.createInputSliderControl("Freq", 0, 20000, self.onFreqChanged, numberVar=self.frequencyVariable)
        sliderFreq.pack(side=tk.LEFT, fill=tk.Y)
        sliderAmp = self.createInputSliderControl("Amp/Mod", 0, 20, self.onAmpChanged, self.operator.amplitude)
        sliderAmp.pack(side=tk.LEFT, fill=tk.Y)
        sliderPhase = self.createInputSliderControl("Phase", 0, 360, self.onPhaseChanged, self.operator.phase)
        sliderPhase.pack(side=tk.LEFT, fill=tk.Y)

        buttonarea = self.createButtonArea()
        buttonarea.pack(side=tk.LEFT, fill=tk.Y)

        self.curvePlot = CurvePlotFrame(self, lineColor=color)
        self.curvePlot.hideAxis()
        self.curvePlot.pack(side=tk.RIGHT, fill=tk.BOTH)

        self.envelopePlot = EnvelopePlotFrame(self, envelopeChanged=self.onEnvelopeChanged)

        self.envelopeActive = False
        self.duration = duration
        self.samplerate = samplerate

        self.savedPoints = []

        self.plotData()

    def getOperator(self):
        return self.operator

    def getAmplitude(self):
        return self.operator.amplitude

    def setMaxAmplitude(self, value):
        self.curvePlot.setYAxisRange(-value, value)

    def setDuration(self, value):
        self.duration = value
        self.onWaveChanged()

    def setFrequency(self, value):
        self.operator.frequency = value
        self.frequencyVariable.set(value)
        self.onWaveChanged()

    def disableEnvelope(self):
        self.envelopeBtn.config(state=tk.DISABLED)
        self.savedPoints = self.envelope.getPoints()
        self.envelope.setPoints([])
        self.showCurvePlot()
        self.onWaveChanged()

    def enableEnvelope(self):
        self.envelopeBtn.config(state=tk.NORMAL)
        self.envelope.setPoints(self.savedPoints)
        self.onWaveChanged()

    def getData(self, time):
        return self.operator.generateWave(time)

    def onAmpChanged(self, value):
        self.operator.amplitude = value
        self.onWaveChanged()

    def onFreqChanged(self, value):
        self.operator.frequency = value
        self.onWaveChanged()

    def onPhaseChanged(self, value):
        self.operator.phase = value
        self.onWaveChanged()

    def generateTimeArray(self):
        return np.linspace(0, self.duration, int(self.duration * self.samplerate))

    def plotData(self):
        time = self.generateTimeArray()
        wave = self.getData(time)
        self.curvePlot.plotData(time, wave)

    def onWaveChanged(self):
        self.plotData()
        self.onChange(self)

    def onEnvelopeChanged(self, points):
        self.envelope.setPoints(points)
        self.onWaveChanged()

    def createSlider(self, frame, from_, to, variable):
        return tk.Scale(frame, from_=to, to=from_, orient=tk.VERTICAL, variable=variable, showvalue=False, digits=2)

    def createEntry(self, frame, min, max, variable):
        return NumericEntry(frame, numericvariable=variable, width=len(str(max)), min=min, max=max)

    def createInputSliderControl(
        self,
        title,
        min,
        max,
        callback,
        default: float = 0,
        type=float,
        numberVar: tk.DoubleVar | tk.IntVar | None = None
    ):
        frame = tk.Frame(self)
        lbl = tk.Label(frame, text=title)
        lbl.pack(side=tk.TOP)

        if (numberVar is None):
            numberVar = tk.DoubleVar(
                value=default) if type == float else tk.IntVar(
                value=int(default))  # set min max here
        numberVar.trace_add("write", lambda *args: callback(type(numberVar.get())))

        slider = self.createSlider(frame, min, max, numberVar)
        slider.pack(side=tk.TOP, fill=tk.Y, expand=True)

        entry = self.createEntry(frame, min, max, numberVar)
        entry.pack(side=tk.BOTTOM, fill=tk.X)

        return frame

    def createButtonArea(self):
        frame = tk.Frame(self)
        imagepath = "images/envelope.png"
        image = tk.PhotoImage(file=imagepath)
        self.envelopeBtn = tk.Button(frame, image=image, command=self.toggleEnvelope)
        self.envelopeBtn.image = image  # type: ignore
        self.envelopeBtn.pack(side=tk.TOP, pady=2, expand=True)
        return frame

    def showEnvelopeFrame(self):
        self.curvePlot.pack_forget()
        self.envelopeBtn.config(relief=tk.SUNKEN)
        self.envelopePlot.pack(side=tk.RIGHT, fill=tk.BOTH)

    def showCurvePlot(self):
        self.envelopePlot.pack_forget()
        self.envelopeBtn.config(relief=tk.RAISED)
        self.curvePlot.pack(side=tk.RIGHT, fill=tk.BOTH)

    def toggleEnvelope(self):
        if (self.envelopeActive):
            self.showCurvePlot()
        else:
            self.showEnvelopeFrame()
        self.envelopeActive = not self.envelopeActive
