import tkinter as tk
from tkinter import simpledialog


class WaveSelectionDialog(simpledialog.Dialog):
    def body(self, master):
        waves = [
            "sine",
            "sawtooth",
            "square",
            "triangle"
        ]

        self.selectedWave = tk.StringVar(value=waves[0])
        tk.Label(master, text="Wähle einen Operator für FM-Synthese aus:").pack()

        frame = tk.Frame(master)
        frame.pack(side=tk.LEFT, padx=10)

        for wave in waves:
            self.createBtn(frame,  wave)

    def createBtn(self, frame,  waveName):
        imagepath = "images/" + waveName + ".png"
        image = tk.PhotoImage(file=imagepath)
        btn = tk.Radiobutton(frame, compound=tk.LEFT, text=waveName, image=image, variable=self.selectedWave, value=waveName)
        btn.image = image # type: ignore
        btn.pack(anchor="w")

    def apply(self):
        self.result = self.selectedWave.get()
