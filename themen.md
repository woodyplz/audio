## Grundlagen

- keine Filter
- multiplikation/addition von mehreren Kurven
- carrier/modulator(1-n)
    - Trägerfrequenz, Modulationsfrequenz
    - Operatoren


## Todo:

- Obertonspektrum (Klavier oder so)
- Nyquist-Shannon-Abtasttheorem
- Einfache und Komplexe Synthese:
    - Parallelschaltung und Kaskadenschaltung (Algorithmus)
- Modulationsintensität
- Verhältnis Frequenz Modulator zu Carrier
- Hüllkurve (Enveloping)
- LFO (Low Frequency Oscilator)

- Welche Frequenz welcher Ton: Tonhöhe
- Effekte (Amp, Hall, EQ,...)


## Literatur:
- THe Scientist and Engineer's Guide to Digital Signal Processing (dspguide.com) 
-  André Ruschkowski. Elektronische Klänge und musikalische Entdeckungen. Reclam, 2010.
- DX7 Anleitung
- Originalpaper FM-Synthese?
- Casio PD, Böhm HDS