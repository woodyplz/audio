import math
import numpy as np
from Algorithm import Algorithm


class DataProvider():
    def __init__(self, algorithm=Algorithm(), samplerate=44100):
        self.algorithm = algorithm
        self.samplerate = samplerate
        self.lastSample = 0

    def reset(self):
        self.lastSample = 0

    def getData(self, samples):
        endsample = self.lastSample + samples
        time = np.arange(self.lastSample, self.lastSample + samples) / self.samplerate
        self.lastSample = endsample

        data = self.algorithm.process(time)
        return data
