import numpy as np


class Envelope:
    def __init__(self, points=[]):
        self.points = points

    def getData(self, samples):
        lastPoint = self.points[0]
        y = []
        for point in self.points[1:-1]:
            length = point.X - lastPoint.X
            values = np.linspace(lastPoint.Y, point.Y, int(samples * length))
            y = np.concatenate((y, values))
            lastPoint = point

        # due to int cast we are missing need to special handling for last curvee
        values = np.linspace(lastPoint.Y, self.points[-1].Y, samples - len(y))
        y = np.concatenate((y, values))

        return y

    def setPoints(self, points):
        self.points = points

    def getPoints(self):
        return self.points

    def apply(self, wave):
        if (len(self.points) == 0):
            return wave
        envelope = self.getData(len(wave))
        return wave * envelope
