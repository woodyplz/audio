from Operator import FMOperator
from typing import List


class Algorithm:
    def __init__(self, operators:List[FMOperator]=[]):
        self.operators = operators

    def process(self, time):
        raise NotImplementedError("Subclasses should implement this method")

    def getFrequency(self):
        operators = self.operators
        carrier = operators[0]
        return carrier.getFrequency()

class SeriesAlgorithm(Algorithm):
    def process(self, time):
        operators = self.operators[::-1] # reversed
        firstOperator = operators[0]
        wave = firstOperator.generateWave(time) # innermost wave first
        for operator in operators[1:]:
            wave = operator.modulate(time, wave)
            
        return wave


class Algorithm1(Algorithm):
    def process(self, time):
        operators: List[FMOperator] = self.operators

        op1 = operators[0]
        op2 = operators[1]
        op3 = operators[2]
        op4 = operators[3]
        wave = op1.modulate(
            time,
            op2.modulate(
                time,
                op3.modulate(
                    time,
                    op4.modulate(time,
                        op4.generateWave(time)
                    )
                )
            )
        )

        return wave


class Algorithm2(Algorithm):
    def process(self, time):
        operators: List[FMOperator] = self.operators

        op1 = operators[0]
        op2 = operators[1]
        op3 = operators[2]
        op4 = operators[3]

        return op1.modulate(
            time,
            op2.modulate(
                time,
                op3.generateWave(time),
                op4.modulate(
                    time,
                    op4.generateWave(time)
                )
            )
        )


class Algorithm3(Algorithm):
    def process(self, time):
        operators: List[FMOperator] = self.operators

        op1 = operators[0]
        op2 = operators[1]
        op3 = operators[2]
        op4 = operators[3]

        return op1.modulate(
            time,
            op2.modulate(time, op3.generateWave(time)),
            op4.modulate(time, op4.generateWave(time))
        )


class Algorithm4(Algorithm):
    def process(self, time):
        operators: List[FMOperator] = self.operators

        op1 = operators[0]
        op2 = operators[1]
        op3 = operators[2]
        op4 = operators[3]

        return op1.modulate(
            time,
            op2.generateWave(time),
            op3.modulate(
                time,
                op4.modulate(
                    time,
                    op4.generateWave(time)
                )
            )
        )


class Algorithm5(Algorithm):
    def process(self, time):
        operators: List[FMOperator] = self.operators

        op1 = operators[0]
        op2 = operators[1]
        op3 = operators[2]
        op4 = operators[3]

        wave1 = op1.modulate(time, op2.generateWave(time))
        wave2 = op3.modulate(time, op4.modulate(time, op4.generateWave(time)))
        return wave1 + wave2


class Algorithm6(Algorithm):
    def process(self, time):
        operators: List[FMOperator] = self.operators

        op1 = operators[0]
        op2 = operators[1]
        op3 = operators[2]
        op4 = operators[3]

        wave1 = op1.modulate(time, op4.modulate(time, op4.generateWave(time)))
        wave2 = op2.modulate(time, op4.modulate(time, op4.generateWave(time)))
        wave3 = op3.modulate(time, op4.modulate(time, op4.generateWave(time)))
        return wave1 + wave2 + wave3
    
class Algorithm7(Algorithm):
    def process(self, time):
        operators: List[FMOperator] = self.operators

        op1 = operators[0]
        op2 = operators[1]
        op3 = operators[2]
        op4 = operators[3]

        wave1 = op1.generateWave(time)
        wave2 = op2.generateWave(time)
        wave3 = op3.modulate(time, op4.modulate(time, op4.generateWave(time)))
        return wave1 + wave2 + wave3
    
class Algorithm8(Algorithm):
    def process(self, time):
        operators: List[FMOperator] = self.operators

        op1 = operators[0]
        op2 = operators[1]
        op3 = operators[2]
        op4 = operators[3]

        wave1 = op1.generateWave(time)
        wave2 = op2.generateWave(time)
        wave3 = op3.generateWave(time)
        wave4 = op4.modulate(time, op4.modulate(time, op4.generateWave(time)))
        return wave1 + wave2 + wave3 + wave4