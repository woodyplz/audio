import numpy as np
from Envelope import Envelope


class FMOperator:
    def __init__(self, frequency: float, amplitude: float, phase: float, envelope: Envelope):
        self.frequency = frequency
        self.amplitude = amplitude
        self.phase = phase
        self.envelope = envelope

    def getFrequency(self):
        return self.frequency

    def generateWave(self, time):
        raise NotImplementedError("Subclasses should implement this method")

    def modulate(self, time, *waves):
        sumWaves = np.sum(waves, axis=0)
        modulatedWave = np.sin(2 * np.pi * self.frequency * time + sumWaves + self.phase)
        return self.applyEnvelope(self.amplitude * modulatedWave)

    def applyEnvelope(self, wave):
        return self.envelope.apply(wave)


class SineWaveOperator(FMOperator):
    def generateWave(self, time):
        wave = np.sin(2 * np.pi * self.frequency * time + self.phase)
        return self.applyEnvelope(self.amplitude * wave)


class SquareWaveOperator(FMOperator):
    def generateWave(self, time):
        wave = np.sign(np.sin(2 * np.pi * self.frequency * time + self.phase))
        return self.applyEnvelope(self.amplitude * wave)


class SawtoothWaveOperator(FMOperator):
    def generateWave(self, time):
        wave = 2 * (np.mod(time * self.frequency + self.phase / (2 * np.pi), 1.0) - 0.5)
        return self.applyEnvelope(self.amplitude * wave)

class TriangleWaveOperator(FMOperator):
    def generateWave(self, time):
        wave = 2 * np.abs(2 * ((time * self.frequency + self.phase / (2 * np.pi)) - np.floor((time * self.frequency + self.phase / (2 * np.pi)) + 0.5))) - 1
        return self.applyEnvelope(self.amplitude * wave)