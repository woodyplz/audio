import tkinter as tk
from tkinter import simpledialog


class AlgorithmSelectionDialog(simpledialog.Dialog):
    def body(self, master):
        self.selectedAlgorithm = tk.IntVar()
        tk.Label(master, text="Wähle einen Algorithmus für FM-Synthese aus:").pack()

        algorithms = [
            (1, "algorithm1"),
            (2, "algorithm2"),
            (3, "algorithm3"),
            (4, "algorithm4"),
            (5, "algorithm5"),
            (6, "algorithm6"),
            (7, "algorithm7"),
            (8, "algorithm8")
        ]

        leftFrame = tk.Frame(master)
        leftFrame.pack(side=tk.LEFT, padx=10)
        rightFrame = tk.Frame(master)
        rightFrame.pack(side=tk.LEFT, padx=10)

        half = len(algorithms) // 2
        first_half = algorithms[:half]
        second_half = algorithms[half:]

        for algorithm in first_half:
            self.createBtn(leftFrame, algorithm)
        for algorithm in second_half:
            self.createBtn(rightFrame, algorithm)

    def createBtn(self, frame, algorithm):
        id, algorithmName = algorithm
        imagepath = "images/" + algorithmName + ".png"
        image = tk.PhotoImage(file=imagepath)
        btn = tk.Radiobutton(frame, text=algorithmName, image=image, variable=self.selectedAlgorithm, value=id)
        btn.image = image  # type: ignore
        btn.pack(anchor="w")

    def apply(self):
        self.result = self.selectedAlgorithm.get()