import os
import numpy as np
from scipy.io.wavfile import write
from scipy import signal
import matplotlib.pyplot as plt
import sounddevice as sd


def ensure_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def save_plot(ts, ys, title, num_samples, filename):
    plt.xlabel("t")
    plt.ylabel("y")
    plt.title(title)
    plt.plot(ts[:num_samples], ys[:num_samples])
    plt.savefig(filename)
    plt.close()

output_directory = "dist"
ensure_dir(output_directory)

AUDIO_RATE = 44100
freq = 440
length = 1

t = np.linspace(0, length, length * AUDIO_RATE, dtype=np.float32)

y = np.sin(2 * np.pi * freq * t)
write("dist/sine.wav", AUDIO_RATE, y)
save_plot(t, y, "Sine Signal", 512, "dist/sine.png")

y = signal.square(2 * np.pi * freq * t)
write("dist/square.wav", AUDIO_RATE, y)
save_plot(t, y, "Square Signal", 512, "dist/square.png")

y = signal.sawtooth(2 * np.pi * freq * t)
write("dist/sawtooth.wav", AUDIO_RATE, y)
save_plot(t, y, "Sawtooth Signal", 512, "dist/sawtooth.png")

y = signal.chirp(t, freq, length, freq * 2)
write("dist/chirp.wav", AUDIO_RATE, y)
save_plot(t, y, "Chirp Signal", 512, "dist/chirp.png")

sd.play(y, AUDIO_RATE)
sd.wait()
