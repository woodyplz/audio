
import tkinter as tk


class NumericEntry(tk.Entry):
    def __init__(self, master=None, textvariable=None, numericvariable:tk.DoubleVar|tk.IntVar|None=None, min=None, max=None, **kwargs):
        # can't use default assignment
        if (textvariable is None):
            textvariable = tk.StringVar(master)
        if (numericvariable is None):
            numericvariable = tk.DoubleVar(master)
        super().__init__(master, textvariable=textvariable, **kwargs)
        self.max = max
        self.min = min
        self.textvariable = textvariable
        self.numericvariable = numericvariable

        self.textvariable.set(str(numericvariable.get()))
        numericvariable.trace_add("write", self.numericvariableChanged)
        self.type = float if isinstance(numericvariable, tk.DoubleVar) else int
        self.validateCommand = self.register(self.validateInput)
        self.config(validate="all", validatecommand=(self.validateCommand, "%P"))

        self.bind("<Up>", self.increment)
        self.bind("<Down>", self.decrement)
        self.bind("<FocusOut>", self.confirm)
        self.bind("<Return>", self.confirm)
        
    def getValue(self):
        value = self.textvariable.get()
        if value == "":
            return 0
        try:
            return self.type(value)
        except:
            return self.numericvariable.get()
        
    def setValue(self, value):
        if (self.max is not None):
            value = min(value, self.max)
        if (self.min is not None):
            value = max(value, self.min)
        self.numericvariable.set(value)
    
    def numericvariableChanged(self, *args):
        numericValue = self.numericvariable.get()
        self.textvariable.set(str(numericValue))

    def increment(self, event=None):
        currentValue = self.getValue()
        value = currentValue + 1
        self.setValue(value)

    def decrement(self, event=None):
        currentValue = self.getValue()
        value = currentValue - 1
        self.setValue(value)

    def validateInput(self, input):
        if input == "":
            return True
        if " " in input: return False
        try:
            self.type(input)
            return True
        except ValueError:
            return False

    def confirm(self, input):
        numericValue = self.getValue()
        self.setValue(numericValue)
