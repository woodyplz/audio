import os
from typing import List
from scipy.io.wavfile import write
import numpy as np
import sounddevice as sd
import tkinter as tk
from CurveConfigurator import CurveConfigurator
import matplotlib.pyplot as plt
from CurvePlot import CurvePlotFrame
from AlgorithmDialog import AlgorithmSelectionDialog
from Dataprovider import DataProvider
from WaveDialog import WaveSelectionDialog
from Operator import SawtoothWaveOperator, SineWaveOperator, SquareWaveOperator, TriangleWaveOperator
from Algorithm import Algorithm, SeriesAlgorithm, Algorithm1, Algorithm2, Algorithm3, Algorithm4, Algorithm5, Algorithm6, Algorithm7, Algorithm8
from NumericEntry import NumericEntry

default_duration = 1
default_samplerate = 44100

default_plotHeight = 200
colors = [
    "#ECEE81",
    "#8DDFCB",
    "#82A0D8",
    "#EDB7ED",
    "#6DC5D1",
    "#FDE49E",
    "#FEB941",
    "#DD761C",
]

maxPlots = min(10, len(colors))


class Application(tk.Frame):
    def __init__(self, master: tk.Tk):
        tk.Frame.__init__(self, master)
        master.protocol("WM_DELETE_WINDOW", self.onClose)
        master.bind("<space>", self.togglePlay)

        self.loop = True
        self.duration = default_duration
        self.savedDuration = self.duration
        self.selectedAlgorithmType = SeriesAlgorithm

        self.headerFrame = tk.Frame(master)
        self.headerFrame.pack(fill=tk.X, side=tk.TOP)

        self.footerFrame = tk.Frame(master)
        self.footerFrame.pack(fill=tk.X, side=tk.BOTTOM)

        self.panel = tk.PanedWindow(master, orient=tk.VERTICAL)
        self.panel.pack(fill=tk.BOTH, side=tk.BOTTOM, expand=True)

        self.curveFrame = tk.Frame(self.panel)
        self.curveFrame.pack(fill=tk.BOTH, expand=True, side=tk.TOP)

        self.resultFrame = tk.Frame(self.panel)
        self.resultFrame.pack(fill=tk.BOTH)

        self.spectrumFrame = tk.Frame(self.panel)
        self.spectrumFrame.pack(fill=tk.BOTH)

        self.panel.add(self.curveFrame, minsize=300)
        self.panel.add(self.resultFrame, minsize=200)
        self.panel.add(self.spectrumFrame, minsize=100)

        self.curvePlot = self.createResultPlot(self.resultFrame)
        self.spectrumPlot = self.createSpectrumPlot(self.spectrumFrame)

        self.createHeaderArea()
        self.createFooterArea()

        self.stream = sd.OutputStream(
            channels=1,
            callback=self.audioCallback,
            samplerate=default_samplerate,
        )
        self.enableLoop()

    def updateGrid(self):
        curves = self.getCurves()
        curvesCount = len(curves)
        self.resetGridLayout(curves)
        if curvesCount == 1:
            self.adjustGridLayout(curves, 1)
        elif 2 <= curvesCount <= 4:
            self.adjustGridLayout(curves, 2)
        else:
            self.adjustGridLayout(curves, 3)

    def resetGridLayout(self, curves):
        curvesCount = len(curves)
        for row in range(curvesCount):
            self.curveFrame.grid_rowconfigure(row, weight=0)
        for col in range(3):  # Maximal 3 spalten
            self.curveFrame.grid_columnconfigure(col, weight=0)

        for curve in curves:
            curve.grid_forget()

    def clearCurves(self):
        curves = self.getCurves()
        for curve in curves:
            curve.destroy()

    def adjustGridLayout(self, curves, columnCount):
        for idx, frame in enumerate(curves):
            row, col = divmod(idx, columnCount)
            frame.grid(row=row, column=col, sticky=tk.NSEW, padx=2, pady=2)
            # column/rowconfigure is only necessary for each column/row but this is less code
            self.curveFrame.grid_columnconfigure(col, weight=1)
            self.curveFrame.grid_rowconfigure(row, weight=1)

    def createHeaderArea(self):
        curveAddBtn = tk.Button(self.headerFrame, text="+", command=self.addCurve)
        curveAddBtn.pack(side=tk.LEFT, padx=2)
        curveRemoveBtn = tk.Button(self.headerFrame, text="-", command=self.removeCurve)
        curveRemoveBtn.pack(side=tk.LEFT, padx=2)

        algorithmBtn = tk.Button(self.headerFrame, text="▧", command=self.openAlgorithmDialog)
        algorithmBtn.pack(side=tk.LEFT, padx=10)

        self.saveBtn = tk.Button(self.headerFrame, text="💾", command=self.saveFile)
        self.saveBtn.pack(side=tk.LEFT, padx=10)

        playBtn = tk.Button(self.headerFrame, text="⏵", command=self.play)
        playBtn.pack(side=tk.RIGHT, padx=2)

        stopBtn = tk.Button(self.headerFrame, text="⏸", command=self.stop)
        stopBtn.pack(side=tk.RIGHT, padx=2)

        self.loopBtn = tk.Button(self.headerFrame, text="↺", command=self.toggleLoop)
        self.loopBtn.pack(side=tk.RIGHT, padx=2)

        durationInputLbl = tk.Label(self.headerFrame, text="s")
        durationInputLbl.pack(side=tk.RIGHT, padx=2)

        durationVar = tk.DoubleVar(name="durationvar", value=self.duration)

        def onDurationVarSet(*args):
            value = max(durationVar.get(), 0.01)
            self.setDuration(value)
            self.updateResultPlot()

        durationVar.trace_add("write", onDurationVarSet)

        self.durationInput = NumericEntry(self.headerFrame, numericvariable=durationVar, justify=tk.RIGHT, min=0, max=10)
        self.durationInput.pack(side=tk.RIGHT)

        self.algorithm = Algorithm()
        self.dataprovider = DataProvider(self.algorithm)

    def createFooterArea(self):
        keys = ["C", "D", "E", "F", "G", "A", "H"]
        for c in keys:
            btn = self.createKeyboardBtn(c)
            btn.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

    def createKeyboardBtn(self, key):
        return tk.Button(self.footerFrame, text=key, command=lambda: self.playKey(key))

    def getFrequencyFromNote(self, note) -> float:
        match note:
            case "C":
                return 261.63
            case "D":
                return 293.66
            case "E":
                return 329.63
            case "F":
                return 349.23
            case "G":
                return 392.00
            case "A":
                return 440.00
            case "H":
                return 493.88
            case _:
                return 440  # won't happen anyways

    def playKey(self, note):
        frequency = self.getFrequencyFromNote(note)
        carrier = self.getCarrier()
        carrier.setFrequency(frequency)

    def getCarrier(self):
        return self.getCurves()[0]

    def setDuration(self, value):
        self.duration = value
        for curve in self.getCurves():
            curve.setDuration(value)

    def openAlgorithmDialog(self):
        dialog = AlgorithmSelectionDialog(root)
        selectedAlgorithm = dialog.result
        algorithmtype = self.getAlgorithmTypeById(selectedAlgorithm)
        self.selectAlgorithm(algorithmtype)

    def selectAlgorithm(self, algorithmtype):
        self.selectedAlgorithmType = algorithmtype
        self.enforce4Curves()
        self.onAlgorithmChanged()

    def enforce4Curves(self):
        while len(self.getCurves()) != 4:
            if len(self.getCurves()) < 4:
                self.addCurveOfType(SineWaveOperator)
            else:
                self.removeCurve()

    def getAlgorithmTypeById(self, selectionvalue):
        match(selectionvalue):
            case 1: return Algorithm1
            case 2: return Algorithm2
            case 3: return Algorithm3
            case 4: return Algorithm4
            case 5: return Algorithm5
            case 6: return Algorithm6
            case 7: return Algorithm7
            case 8: return Algorithm8
            case _: return SeriesAlgorithm

    def ensureDir(self, directory):
        if not os.path.exists(directory):
            os.makedirs(directory)

    def saveFile(self):
        if (len(self.getCurves()) <= 0): return
        output_directory = "dist"
        self.ensureDir(output_directory)
        data = self.getResultData()
        write("dist/data.wav", default_samplerate, data)

    def onCurveChanged(self, curve):
        self.updateResultPlot()
        amp = self.getMaxAmplitude()
        self.setMaxAmplitude(amp)

    def onAlgorithmChanged(self):
        self.setAlgorithm(self.selectedAlgorithmType)
        self.dataprovider = DataProvider(self.algorithm)
        self.updateResultPlot()

    def setAlgorithm(self, algorithmtype):
        curves = self.getCurves()
        operators = [x.getOperator() for x in curves]
        if len(curves) != 4: algorithmtype = SeriesAlgorithm
        self.algorithm = algorithmtype(operators)

    def getResultData(self):
        samples = self.getSamplesFromDuration(self.duration)
        self.dataprovider.reset()
        return self.dataprovider.getData(samples)

    def updateResultPlot(self):
        if (len(self.getCurves()) == 0):
            self.plotData([], [])
            return
        time = np.linspace(0, self.duration, int(self.duration * default_samplerate))
        wave = self.algorithm.process(time)
        self.plotData(time, wave)
        self.plotSpectrum(wave, default_samplerate)

    def getMaxAmplitude(self):
        maxAmp = 0
        for curve in self.getCurves():
            maxAmp = max(curve.getAmplitude(), maxAmp)
        return maxAmp

    def setMaxAmplitude(self, value):
        for curve in self.getCurves():
            curve.setMaxAmplitude(value)

    def addCurve(self):
        curveCount = len(self.getCurves())
        if (curveCount >= maxPlots):
            return

        dialog = WaveSelectionDialog(root)
        if dialog.result == "sine":
            operatorType = SineWaveOperator
        elif dialog.result == "sawtooth":
            operatorType = SawtoothWaveOperator
        elif dialog.result == "square":
            operatorType = SquareWaveOperator
        elif dialog.result == "triangle":
            operatorType = TriangleWaveOperator
        else:
            return
        
        self.addCurveOfType(operatorType)

    def addCurveOfType(self, operatorType):
        curveCount = len(self.getCurves())
        if (curveCount >= maxPlots):
            return
        color = colors[curveCount-1]
        curve = CurveConfigurator(
            self.curveFrame,
            operatorType,
            self.duration,
            default_samplerate,
            self.onCurveChanged,
            color=color
        )
        curve.grid(sticky=tk.NSEW)  # temporary, will be adjusted by updateGrid
        self.updateGrid()
        self.onAlgorithmChanged()
        if (self.loop):
            curve.disableEnvelope()
        else:
            curve.enableEnvelope()

    def removeCurve(self):
        curves = self.getCurves()
        if (len(curves) == 0):
            return
        curves[len(curves) - 1].destroy()
        self.updateGrid()
        self.onAlgorithmChanged()

    def getCurves(self) -> List[CurveConfigurator]:
        return self.curveFrame.winfo_children()  # type: ignore

    def createResultPlot(self, frame):
        plot = CurvePlotFrame(frame, lineColor="black", lineWidth=1)
        plot.pack(fill=tk.BOTH, pady=2)
        plot.setLabels("Time", "Amplitude")
        return plot

    def createSpectrumPlot(self, frame):
        plot = CurvePlotFrame(frame, lineColor="blue", lineWidth=2)
        plot.pack(fill=tk.BOTH, pady=2)
        plot.setLabels("Frequency", "Amplitude")
        return plot

    def plotData(self, x, y):
        self.curvePlot.plotData(x, y)

    def plotSpectrum(self, signal, samplerate):
        n = len(signal)
        fourierTransformation = np.fft.fft(signal)
        fourierTransformation = fourierTransformation[:n // 2]

        frequencies = np.fft.fftfreq(n, d=1/samplerate)
        frequencies = frequencies[:n // 2]
        amplitudes = np.abs(fourierTransformation) / n
        self.spectrumPlot.plotData(frequencies, amplitudes)

    def playFixed(self):
        data = self.getResultData()
        self.dataprovider.reset()
        sd.play(data, default_samplerate)
        sd.wait()

    def playStream(self):
        if self.stream.active:
            return
        self.dataprovider.reset()
        self.stream.start()

    def togglePlay(self, event=None):
        if (not self.stream.active):
            self.play()
        else:
            self.stop()

    def play(self):
        if (len(self.getCurves()) == 0):
            return
        self.stream.stop()
        if self.loop:
            self.playStream()
        else:
            self.playFixed()

    def getSamplesFromDuration(self, duration):
        return int(duration * default_samplerate)

    def audioCallback(self, outdata, samples, time, status):
        wave = self.dataprovider.getData(samples)
        outdata[:] = np.array(wave).reshape(-1, 1)

    def stop(self):
        self.stream.stop()

    def toggleLoop(self):
        self.stop()
        if self.loop:
            self.disableLoop()
        else:
            self.enableLoop()

    def disableLoop(self):
        self.loop = False
        self.loopBtn.config(relief=tk.RAISED)
        self.durationInput.config(state=tk.NORMAL)
        self.saveBtn.config(state=tk.NORMAL)
        self.setDuration(self.savedDuration)
        curves = self.getCurves()
        for curve in curves:
            curve.enableEnvelope()

    def enableLoop(self):
        self.loop = True
        self.loopBtn.config(relief=tk.SUNKEN)
        self.savedDuration = self.duration
        self.setDuration(0.05)
        self.durationInput.config(state=tk.DISABLED)
        self.saveBtn.config(state=tk.DISABLED)

        curves = self.getCurves()
        for curve in curves:
            curve.disableEnvelope()

    def onClose(self):
        self.quit()
        self.master.destroy()


root = tk.Tk()
root.title("FM-Synthese")
root.geometry("1280x768")
app = Application(master=root)
app.mainloop()
