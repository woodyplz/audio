import tkinter as tk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np


class EnvelopePlotFrame(tk.Frame):
    def __init__(self, master=None, lineColor=None, lineWidth=1, envelopeChanged=lambda points: None):
        super().__init__(master)
        self.lineColor = lineColor
        self.lineWidth = lineWidth
        self.envelopeChanged = envelopeChanged
        fig = plt.figure()
        self.ax = fig.add_subplot()
        self.hideAxis()

        self.canvas = FigureCanvasTkAgg(fig, master=self.master)

        self.points = [
            Point(0, 0),
            Point(0, 1),
            Point(0.5, 1),
            Point(1, 1),
            Point(1, 0)
        ]
        self.selectedPoint = None
        self.moveEventCbId: int | None = None
        self.canvas.mpl_connect("button_press_event", self.onPress)
        self.canvas.mpl_connect("button_release_event", self.onRelease)
        self.drawPoints()

    def drawPoints(self):
        self.ax.clear()
        self.ax.set_xlim((0, 1))
        self.ax.set_ylim((0, 1))
        x_values = [p.X for p in self.points]
        y_values = [p.Y for p in self.points]
        self.ax.plot(x_values, y_values, "b-", marker="o", markersize=5, picker=10)
        self.ax.grid(True, color="gray", linestyle="-", linewidth=0.5)
        self.canvas.draw()

    def onPress(self, event):
        if event.inaxes != self.ax:
            return
        for point in self.points[1:-1]:
            if np.hypot(point.X - event.xdata, point.Y - event.ydata) < 0.05:
                self.selectedPoint = point
                self.moveEventCbId = self.canvas.mpl_connect("motion_notify_event", self.onMotion)
                break

    def onMotion(self, event):
        if self.selectedPoint is None or event.inaxes != self.ax:
            return

        x = np.clip(event.xdata, 0, 1)
        y = np.clip(event.ydata, 0, 1)

        index = self.points.index(self.selectedPoint)

        for p in self.points[1:index]:
            p.X = min(p.X, x)

        for p in self.points[index:-1]:
            p.X = max(p.X, x)

        self.selectedPoint.X = x
        self.selectedPoint.Y = y

        self.drawPoints()

    def onRelease(self, event):
        self.canvas.mpl_disconnect(self.moveEventCbId)
        self.moveEventCbId = None
        self.onChange()

    def onChange(self):
        self.envelopeChanged(self.points)

    def hideAxis(self):
        self.ax.set_position([0, 0, 1, 1])

    # todo: refactor to subclass
    def pack(self, *args, **kwargs):
        self.canvas.get_tk_widget().pack(fill=tk.BOTH, expand=True)
        super().pack(*args, **kwargs)

    def pack_forget(self, *args, **kwargs):
        self.canvas.get_tk_widget().pack_forget()
        super().pack_forget(*args, **kwargs)


class Point:
    def __init__(self, x: float = 0, y: float = 0):
        self.X = x
        self.Y = y
