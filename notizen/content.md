# Intro

## Historie

Eingehen auf FM ohne Sound?

Mitte 1960 Don Buchla implementiert analoge FM-Synthese, verwendete spannungsgesteuerte Oszillatoren (VCOs)
war aber nicht so stabil und präzise wie Chowning

Chowning hat 1967 die digitale FM-Synthese an der Stanford University entdeckt. 
[Bild chowning?]
Die eigentliche Innovation war die Beziehungen zwischen verschiedener Frequenzen zu finden, welche es erlaubt haben verschieden Klangfarben zu generieren.
Er hat ein theoretisches Framework und praktische Algorithmen gefunden/erfunden, welche echte Instrumente generieren konnten.

1974 Patent an Yamaha lizenziert, welche den berühmten DX7 gebaut haben
[Bild]

## Anwendungsbereiche in Musik + Sounddesign

Könnten Sounds von DX7 sounds spielen?

Klanggestaltung in Pop-Musik
Beispiel "Take On Me" von a-ha
Bass am Anfang
[Song] 

Elektronische Musik
"Just Can't Get Enough" von Depeche Mode.

Spieleindustrie
Soundtracks von frühen Videospielen wie "Sonic the Hedgehog".
in Songchips verwendet

# Grundlagen von Ton und Obertönen

## Tonwellen und Frequenzen

Sinuskurve zeigen (aus unserem Tool ?)
Formel zeigen:  y(t)=Asin(2πft+ϕ) 
Komplexe Wellenform zeigen: (meinst du hiermit andere Funktionen wie sägezahn etc?)
Auch formel zeigen?

Vielleicht erklären, dass es auch eine Periode gibt etc?

Klangbeispiele?
Unterschiede?
Scharfe Kanten etc?    

## Obertöne und Harmonik

[Video]
Obertöne sind die neben dem Grundton mitklingenden Bestandteile eines musikalisch instrumental (außer Schlagzeug) oder vokal erzeugten Tones.
Grundton der gespielt wird hat eine Menge an Obertönen, welche ein **ganzzahliges** Vielfaches der Grundfrequenz sind.
[Bilder Harmonische Reihe]
Diese Teiltöne nennt man Harmonische (1. Harmonische = Grundton)
Gibt auch Untertöne, welches ganzzahlige Teiler der Grundfrequenz sind, jedoch treten die bei fast keinen Instrumenten auf, können aber bei gewissen Streichinstrumenten erzeugt werden.

# Funktionsgeneratoren

Gerät zum Erzeugen von periodische elektrischer Signale mit unterschiedlichen Kurvenformen insbesondere Sinus, Rechteck, Dreieck und Sägezahn, mit einstellbarer Frequenz und Amplitude.
Formeln?
    