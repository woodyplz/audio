Struktur für Präsentation?

#1. Intro
    - Überblick Präsentation
	- Geschichte der FM Synthese
	- Anwendungsbereiche in Musik + Sounddesign
#2. Grundlagen von Ton und Obertönen
	- Tonwellen und Frequenzen
		- Unterschied zwischen einfachen Sinus & komplexen Wellenformen
	- Obertöne und Harmonik
		- Unterschied Grundfrequenz und Obertöne
		- Harmonik + Harmonische Serie
	- Audiobeispiel:
		- Einfacher Sinus
		- Zeigen, wie verschieden Obertöne einen Klang beinflussen (wie?)
		- Unterschied von einfachen & komplexen Wellenformen
#3. Funktionsgeneratoren
	- Funktionsgeneratoren in der Synthese
	- Arten von Funktionsgeneratoren in der FM-Synthese (Sinus, Rechteck, Sägezahn) + Eigenschaften
	- Wie FGs den Klang formen (auswirkungen auf resultierenden Klang)
#4. Grundlagen der FM-Synthese
	- Definition + Grundkonzept
		- Unterschied zur subtraktiven Synthese??? (grob?)
	- Träger + Modulator
		- Wie beeinflusst Modulator den Träger?
		- Operatoren/Algorithmen?
	- Mathematische Grundlage: Wie FM-Synthese komplexe Wellenformen erzeugt
		- Gleichung für einfache Synthese
		- Modulationsindex und seine Auswirkung auf den Klang
	- Demo: 
		- einfache FM-Synthese (1 Modulator)
		- z.B Veränderung von Frequenz & ModIndex zeigen
#5. Komplexe FM-Synthese
	- Einführung Verwendung mehrer Modulatoren
		- Serien- & Kaskadenschaltung, Self-feeding?
	- Algorithmen/Routings:
		- Welche gibt es
		- Funktionsweise
	- Demo: komplexe FM-Synthese (4 Modulatoren)
#6. Live-Demo ?
	- Zeigen typischer Sounds sowie Nachahmung von zB Glocken, Bass, etc
	- Veränderung an Envelope (und Echzeit-Anpassung?)