import tkinter as tk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.transforms as mtransforms

class CurvePlotFrame(tk.Frame):
    def __init__(self, master=None, lineColor=None, lineWidth=1, minY=None, maxY=None):
        super().__init__(master)
        self.lineColor = lineColor
        self.lineWidth = lineWidth
        fig = plt.figure()
        self.ax = fig.add_subplot()
        self.canvas = FigureCanvasTkAgg(fig, master=self.master)

        self.minY = minY
        self.maxY = maxY
        self.labels = None
        self.plotData([], [])

    def hideAxis(self):
        self.ax.set_position([0, 0, 1, 1])

    def setLabels(self, xLabel, yLabel):
        self.labels = (xLabel, yLabel)

    def setYAxisRange(self, min, max):
        self.minY = min
        self.maxY = max
        self.refresh()

    def plotData(self, x, y):
        self.ax.clear()
        self.ax.plot(x, y, linewidth=self.lineWidth, color=self.lineColor)
        self.refresh()

    def refresh(self):
        if (self.labels is not None):
            xLabelTxt = self.labels[0]
            yLabelTxt = self.labels[1]
            self.ax.set_xlabel(xLabelTxt, loc="right")
            self.ax.set_ylabel(yLabelTxt, loc="top")

        if (self.minY is not None and self.maxY is not None):
            self.ax.set_ylim(self.minY, self.maxY)

        self.styleAxis()
        self.canvas.draw()

    def styleAxis(self):
        ax = self.ax
        # Axis description text
        ax.xaxis.label.set_color("gray")
        ax.yaxis.label.set_color("gray")
        # Background grid
        ax.grid(True, color="gray", linestyle="-", linewidth=0.5)  
        ax.tick_params(
            axis="both",
            direction="in",
            length=0,
            colors="gray",
            labelsize=6,
        )

        for tick in ax.get_xticklabels():
            tick.set_horizontalalignment("right")
            tick.set_verticalalignment("baseline") 
            trans = mtransforms.ScaledTranslation(0, 0.055, plt.gcf().dpi_scale_trans)
            tick.set_transform(tick.get_transform() + trans)

        for tick in ax.get_yticklabels():
            tick.set_horizontalalignment("left")
            tick.set_verticalalignment("baseline") 
            trans = mtransforms.ScaledTranslation(0.06, 0.01, plt.gcf().dpi_scale_trans)
            tick.set_transform(tick.get_transform() + trans)

        xlabel = ax.xaxis.label
        xlabel.set_horizontalalignment("left")
        xlabel.set_verticalalignment("baseline")
        trans = mtransforms.ScaledTranslation(0.1, 0.1, plt.gcf().dpi_scale_trans)
        xlabel.set_transform(xlabel.get_transform() + trans)

        # Make all borders gray
        ax.spines[:].set_color("gray")

    # todo: refactor to subclass
    def pack(self, *args, **kwargs):
        self.canvas.get_tk_widget().pack(fill=tk.BOTH, expand=True)
        super().pack(*args, **kwargs)

    def pack_forget(self, *args, **kwargs):
        self.canvas.get_tk_widget().pack_forget()
        super().pack_forget(*args, **kwargs)
